import java.util.*;

class KComp{
	
	private int[] array;
	private int K;
	HashMap<Integer,Integer> hashmap;
	
	public KComp(int[] array, int K){
		this.array = array;
		this.K = K;
		hashmap = new HashMap<Integer,Integer>();
	}
	
	public int numComplementary(){
		
		int count = 0;
		
		for(int i:array){
			if(hashmap.get(i) == null){
				hashmap.put(i,1);
			}else{
				hashmap.put(i,hashmap.get(i)+1);
			}
		}
		
		for(Map.Entry<Integer,Integer> entry: hashmap.entrySet()){
			int needed = K - entry.getKey();
			if(hashmap.containsKey(needed)){
				count += hashmap.get(entry.getKey()) * hashmap.get(needed);
			}
		}
		return count; 
	}
}

public class KComplementary{
	public static void main(String[] args){
		
		int[] array = {1, 2, 3, 3, 4, 5};
		KComp kcomp = new KComp(array, 6);
		System.out.println(kcomp.numComplementary());
	}
}