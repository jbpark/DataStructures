class Node{
	
	private Node next;
	private Node prev;
	private Integer val;
	
	public Node(Integer val){
		next = null;
		prev = null;
		this.val = val;
	}
	
	public void setNext(Node next){
		this.next = next;
	}
	
	public Node getNext(){
		return next;
	}
	
	public void setPrev(Node prev){
		this.prev = prev;
	}
	
	public Node getPrev(){
		return this.prev;
	}
	
	public void setVal(Integer val){
		this.val = val;
	}
	
	public Integer getVal(){
		return this.val;
	}
}

class CreateStack{
	
	private int nItems;
	private Node head;
	private Node tail;
	
	public CreateStack(){
		head = new Node(null);
		tail = new Node(null);
		head.setNext(tail);
		tail.setPrev(head);
		nItems=0;
	}
	
	public void push(Integer val){
		Node new_node = new Node(val);
		Node temp = tail.getPrev();
		temp.setNext(new_node);
		new_node.setPrev(temp);
		new_node.setNext(tail);
		tail.setPrev(new_node);
		nItems++;
	}
	
	public Integer pop(){
		if(isEmpty()){
			System.out.println("Stack is empty");
			return null;
		}else{
			Node temp = tail.getPrev();
			temp.getPrev().setNext(tail);
			tail.setPrev(temp.getPrev());
			nItems--;
			return temp.getVal();
		}
	}
	
	public void peek(){
		if(isEmpty()){
			System.out.println("Stack is empty");
		}else{
			System.out.println(tail.getPrev().getVal());
		}
	}
	
	public boolean isEmpty(){
		return (nItems == 0);
	}
}

public class Stack{
	public static void main(String[] args){
		CreateStack stack = new CreateStack();
		stack.pop();
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.peek();
		stack.pop();
		stack.peek();
		stack.pop();
		stack.peek();
		stack.pop();
		stack.peek();
		stack.pop();
		stack.peek();
	}
}