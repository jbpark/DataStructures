import java.util.*;

class Node{
	
	Node next;
	Node prev;
	Character val;
	
	public Node(Character val){
		next = null;
		prev = null;
		this.val = val;
	}
	
	public void setNext(Node next){
		this.next = next;
	}
	
	public Node getNext(){
		return next;
	}
	
	public void setPrev(Node prev){
		this.prev = prev;
	}
	
	public Node getPrev(){
		return prev;
	}
	
	public void setVal(Character val){
		this.val = val;
	}
	
	public Character getVal(){
		return val;
	}
}

class CreateReverser{
	
	Node head;
	Node tail;
	int nItems;
	
	public CreateReverser(){
		head = new Node(null);
		tail = new Node(null);
		head.setNext(tail);
		tail.setPrev(head);
		nItems=0;
	}
	
	public void add_Char(Character c){
		Node new_node = new Node(c);
		Node temp = tail.getPrev();
		temp.setNext(new_node);
		new_node.setPrev(temp);
		new_node.setNext(tail);
		tail.setPrev(new_node);
		nItems++;
	}
	
	public Character remove_Char(){
		Node temp = tail.getPrev();
		temp.getPrev().setNext(tail);
		tail.setPrev(temp.getPrev());
		return temp.getVal();
	}
	
	public boolean isEmpty(){
		return (nItems == 0);
	}
	
}

public class Reverser{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		CreateReverser reverser = new CreateReverser();
		System.out.println("Enter a statement you would like to revert.");
		String[] text = in.nextLine().split("");
		
		for(int i=0; i<text.length; i++){
			reverser.add_Char(text[i].charAt(0));
		}
		
		System.out.println("The following is the statement reversed:");
		for(int i=0; i<text.length; i++){
			System.out.print(reverser.remove_Char());
		}
		
	}
}