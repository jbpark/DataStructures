class CreateStack{
	
	private int[] stack;
	private int top;
	private int nItems;
	private int max_size;
	
	public CreateStack(int max_size){
		this.max_size = max_size;
		stack = new int[max_size];
		nItems = 0;
		top = 0;
	}
	
	public void push(int item){
		if(isFull()){
			System.out.println("push: Stack is Full!");
		}else{
			stack[top++] = item;
			nItems++;
		}
	}
	
	public int pop(){
		if(isEmpty()){
			System.out.println("pop: Stack is empty!");
			return -1;
		}else{
			nItems--;
			return stack[--top];
		}
	}
	
	public void peek(){
		if(isEmpty()){
			System.out.println("peek: Stack is empty!");
		}else{
			System.out.println(stack[top-1]);
		}
	}
	
	public boolean isFull(){
		return (nItems == max_size);
	}
	
	public boolean isEmpty(){
		return (nItems == 0);
	}
	
}

public class Stack{
	public static void main(String[] args){
		
		CreateStack stack = new CreateStack(5);
		stack.pop();
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.peek();
		stack.pop();
		stack.peek();
		stack.pop();
		stack.peek();
		stack.pop();
		stack.peek();
		stack.pop();
		stack.peek();
	}
}