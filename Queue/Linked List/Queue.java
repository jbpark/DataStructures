class Node{
	
	private Node next;
	private Node prev;
	private Integer data;
	
	public Node(Integer data){
		next = null;
		prev = null;
		this.data = data;
	}
	
	public void setData(Integer data){
		this.data = data;
	}
	
	public Integer getData(){
		return data;
	}
	
	public void setNext(Node node){
		next = node;
	}
	
	public Node getNext(){
		return next;
	}
	
	public void setPrev(Node prev){
		this.prev = prev;
	}
	
	public Node getPrev(){
		return prev;
	}
}

class LinkedList{
	
	private Node head;
	private Node tail;
	private int nItems;
	
	public LinkedList(){
		head = new Node(null);
		tail = new Node(null);
		head.setNext(tail);
		tail.setPrev(head);
		nItems = 0;
	}
	
	public void push(Integer data){
		Node node = new Node(data);
		Node temp = tail.getPrev();
		temp.setNext(node);
		node.setNext(tail);
		node.setPrev(temp);
		tail.setPrev(node);
		nItems++;
	}
	
	public void add(Integer data, int index){
		
		if(index > nItems || index < 1){
			System.out.println("Error!");
		}else{
			Node node = new Node(data);
			Node temp = head;
			for(int i=1; i<index; i++){
				temp = temp.getNext();
			}
			node.setNext(temp.getNext());
			node.setPrev(temp);
			temp.getNext().setPrev(node);
			temp.setNext(node);
			nItems++;
		}
	}
	
	public Integer pop(){
		if(nItems == 0){
			System.out.println("Queue is empty!");
			return null;
		}else{
			Node temp = head.getNext();
			head.setNext(head.getNext().getNext());
			head.getNext().setPrev(head);
			nItems--;
			return temp.getData();
		}
		
	}
	
	public Integer remove(int index){
		if(nItems == 0){
			System.out.println("Queue is empty!");
			return null;
		}else{
			Node traverse = head;
			Node temp;
			for(int i=1; i<index; i++){
				traverse = traverse.getNext();
			}
			temp = traverse.getNext();
			traverse.setNext(temp.getNext());
			temp.getNext().setPrev(traverse);
			nItems--;
			return temp.getData();
		}
	}
	
	public void display_Queue(){
		if(nItems == 0){
			System.out.println("Queue is empty!");
		}else{
			Node traverse = head.getNext();
			for(int i=0; i<nItems; i++){
				System.out.print("[" + traverse.getData() + "] ");
				traverse = traverse.getNext();
			}
			System.out.print("\n");
		}
		
	}
	
	public int size(){
		return nItems;
	}
}


public class Queue{
	public static void main(String[] args){
		LinkedList queue = new LinkedList();
		queue.push(1);
		queue.push(2);
		queue.push(3);
		queue.push(4);
		queue.display_Queue();
		
		queue.pop();
		queue.display_Queue();
		queue.pop();
		queue.display_Queue();
		
		queue.add(1,1);
		queue.display_Queue();
		queue.add(2,2);
		queue.display_Queue();
		queue.remove(3);
		queue.display_Queue();
		queue.remove(1);
		queue.display_Queue();
	}
}