//Implementation of Queue: First-In-First-Out
import java.util.*;

class QueueDemo{
	
	private ArrayList<Integer> queue; 
	
	public QueueDemo(){
		queue = new ArrayList<Integer>();
	}
	
	public void push(int addition){
		queue.add(addition);
		System.out.println("The following is in your queue now:");
		display();
	}
	
	public int pop(){
		try{
			int element = queue.get(0);
			queue.remove(0);
			System.out.println("The following is in your queue now:");
			display();
			return element;
		}catch(Exception e1){
			System.out.println("No elements to pop!");
			return 0;
		}
	}
	
	public void display(){
		for(int i=0; i<queue.size(); i++){
			System.out.print(queue.get(i) + " ");
		}
		System.out.print("\n");
	}
}

public class Queue{
	
	public static void rules(){
		System.out.println("Please use the following commands:");
		System.out.println("1. push element (e.g. push 3)");
		System.out.println("2. pop element (e.g. pop 23)");
	}
	
	public static void main(String[] args){
		QueueDemo queueDemo = new QueueDemo();
		Scanner in = new Scanner(System.in);
		
		rules();
		
		while(true){
			try{
				String[] command = in.nextLine().split(" ");
				if(command[0].equals("push")){
					queueDemo.push(Integer.parseInt(command[1]));
				}else if(command[0].equals("pop")){
					queueDemo.pop();
				}else{
					System.out.print("ERROR! ");
					rules();
				}
			}catch(Exception e){
				System.out.print("ERROR! ");
				rules();
			}
			
		}
		
	}
}