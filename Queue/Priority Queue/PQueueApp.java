class QueueItem{
	
	private int priority;
	private int data;
	private QueueItem next;
	
	public QueueItem(int priority, int data){
		this.priority = priority;
		this.data = data;
	}
	
	public int getPriority(){
		return this.priority;
	}
	
	public int getData(){
		return this.data;
	}
	
	public void setNext(QueueItem next){
		this.next = next;
	}
	
	public QueueItem getNext(){
		return this.next;
	}
}

class PQueue{
	
	private int nItems;
	private QueueItem head;
	
	public PQueue(){
		nItems = 0;
	}
	
	public void insert(QueueItem item){
		if(isEmpty()){
			head = item;
			nItems++;
		}else{
			QueueItem current = head;
			QueueItem prev = current;
			
			while(current.getPriority() < item.getPriority()){
				prev = current;
				current = current.getNext();
				if(current == null){
					break;
				}
			}
			
			if(current == prev){
				item.setNext(head);
				head = item;
			}else{
				prev.setNext(item);
				item.setNext(current);
			}
			nItems++;
		}
	}
	
	public void pop(){
		if(isEmpty()){
			System.err.println("ERROR: Queue is empty!");
		}else{
			System.out.println("Priority: " + head.getPriority() + " Data: " + head.getData() + " has been popped!");
			head = head.getNext();
		}
	}
	
	public boolean isEmpty(){
		return (nItems == 0);
	}
	
	public void display(){
		QueueItem current = head;
		
		while(current != null){
			System.out.println("Priority: " + current.getPriority() + " Data: " + current.getData());
			current = current.getNext();
		}
		
	}
	
}

public class PQueueApp{
	public static void main(String[] args){
		
		PQueue pQueue = new PQueue();
		QueueItem q1 = new QueueItem(5,5);
		QueueItem q2 = new QueueItem(3,5);
		QueueItem q3 = new QueueItem(2,5);
		QueueItem q4 = new QueueItem(7,5);
		QueueItem q5 = new QueueItem(1,5);
		QueueItem q6 = new QueueItem(9,5);
		
		pQueue.insert(q1);
		pQueue.insert(q2);
		pQueue.insert(q3);
		pQueue.insert(q4);
		pQueue.insert(q5);
		pQueue.insert(q6);
		
		pQueue.display();
		
		pQueue.pop();
		pQueue.pop();
		pQueue.pop();
		
		pQueue.display();
		
		
	}
}