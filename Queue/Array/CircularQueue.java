import java.util.*;
import java.lang.Integer;

class DemoQueue{
	
	private int maxSize;
	private int[] queArray;
	private int front;
	private int rear;
	private int nItems;
	
	public DemoQueue(int maxSize){
		this.maxSize = maxSize;
		queArray = new int[maxSize];
		front = 0;
		rear = -1;
		nItems = 0;
	}
	
	
	
	public void insert(int item){
		if(rear == maxSize-1){
			rear = -1;
		}
		queArray[++rear] = item;
		nItems++;
	}
	
	public long remove(){
		int temp = queArray[front++];
		if(front == maxSize){
			front = 0;
		}
		nItems--;
		return temp;
	}
	
	public Integer peek(){
		if(isEmpty()){
			System.out.println("Queue is empty!");
			return null;
		}else{
			System.out.println(queArray[front]);
			return queArray[front];
		}
	}
	
	public boolean isEmpty(){
		return (nItems == 0);
	}
	
	public boolean isFull(){
		return (nItems == maxSize);
	}
	
	public int size(){
		return nItems;
	}
}

public class CircularQueue{

	public static void rules(){
		System.out.println("Please use the following commands:");
		System.out.println("1. push element (e.g. push 3)");
		System.out.println("2. pop element");
		System.out.println("3. peek");
	}
	
	public static void main(String[] args){
		DemoQueue queue = new DemoQueue(5);
		
		Scanner in = new Scanner(System.in);
		
		rules();
		
		while(true){
			try{
				String[] command = in.nextLine().split(" ");
				if(command[0].equals("push")){
					queue.insert(Integer.parseInt(command[1]));
				}else if(command[0].equals("pop")){
					queue.remove();
				}else if(command[0].equals("peek")){
					queue.peek();
				}else{
					System.out.print("ERROR! ");
					rules();
				}
			}catch(Exception e){
				System.out.print("ERROR! ");
				rules();
			}
			
		}
		
	}
}