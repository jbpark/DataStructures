import java.util.*;

class CreateQueue{
	
	private int[] queue;
	private int maxSize;
	private int head;
	private int rear;
	private int nItems;
	
	public CreateQueue(int maxSize){
		queue = new int[maxSize];
		this.maxSize = maxSize;
		head = 0;
		rear = 0;
		nItems = 0;
	}
	
	public void push(int item){
		if(isFull()){
			System.out.println("Queue is full!");
		}else{
			if(rear == maxSize){
				rear = 0;
			}
			queue[rear++] = item;
			nItems++;
		}
	}
	
	public Integer pop(){
		if(isEmpty()){
			System.out.println("Queue is empty!");
			return null;
		}else{
			if(head == maxSize){
				head=0;
			}
			int tmp = queue[head++];
			nItems--;
			return tmp;
		}
	}
	
	public Integer peek(){
		if(isEmpty()){
			System.out.println("Queue is empty!");
			return null;
		}else{
			System.out.println("The head value is: " + queue[head]);
			return queue[head];
		}
	}
	
	public boolean isEmpty(){
		return (nItems == 0);
	}

	public boolean isFull(){
		return (nItems == maxSize);
	}
}

public class Queue{
	
	public static void rules(){
		System.out.println("Please use the following commands:");
		System.out.println("1. push element (e.g. push 3)");
		System.out.println("2. pop element");
		System.out.println("3. peek");
		System.out.println("4. end");
	}
	
	public static void main(String[] args){
		
		Scanner in = new Scanner(System.in);
		
		CreateQueue queue = new CreateQueue(5);
		rules();
		while(true){
			try{
				String[] command = in.nextLine().split(" ");
				if(command[0].equals("push")){
					queue.push(Integer.parseInt(command[1]));
				} else if(command[0].equals("pop")){
					queue.pop();
				} else if(command[0].equals("peek")){
					queue.peek();
				} else if(command[0].equals("end")){
					break;
				} else{
					System.out.print("Error! ");
					rules();
				}
			} catch(Exception e){
				System.out.print("Error! ");
				rules();
			}
		}
		
		
		
	}
}