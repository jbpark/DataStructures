import java.util.*;

class DataItem{
	
	private int key;
	private int data;
	private DataItem next;
	
	public DataItem(int data){
		key = data;
		this.data = data;
	}
	
	public int getKey(){
		return key;
	}
	
	public int getData(){
		return data;
	}
	
	public void setNext(DataItem item){
		next = item;
	}
	
	public DataItem getNext(){
		return next;
	}
	
}

class HTable{
	
	private DataItem[] hTable;
	private int mSize;
	private int nItems;
	
	public HTable(int mSize){
		this.mSize = mSize;
		hTable = new DataItem[mSize];
		nItems = 0;
	}
	
	public void insert(DataItem item){
		
		int index = hash(item.getKey());
		DataItem current = hTable[index];
		DataItem prev = current;
		while(current != null){
			prev = current;
			current = current.getNext();
		}
		if(current == prev){
			hTable[index] = item;
		}else{
			prev.setNext(item);
		}
	}
	
	public DataItem find(DataItem item){
		int index = hash(item.getKey());
		DataItem current = hTable[index];
		while(current != item){
			if(current == null){
				return null;
			}else{
				current = current.getNext();
			}
		}
		return current;
	}
	
	public DataItem delete(DataItem item){
		int index = hash(item.getKey());
		DataItem current = hTable[index];
		DataItem prev = current;
		while(current != item){
			if(current == null){
				return null;
			}else{
				prev = current;
				current = current.getNext();
			}
		}
		if(current == prev){
			hTable[index] = current.getNext();
		}else{
			prev.setNext(current.getNext());
		}
		return current;
	}
	
	public int hash(int key){
		return key%mSize;
	}
	
	public void display(){
		
		DataItem current;
		
		for(int i=0; i<mSize; i++){
			current = hTable[i];
			while(current != null){
				System.out.println(i + ": " + current.getData());
				current = current.getNext();
			}
		}
	}
	
}


public class HashTableApp{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		int arraySize;
		
		System.out.println("Enter size of hash array:");
		arraySize = Integer.parseInt(in.nextLine());
		
		HTable hashTable = new HTable(arraySize);
		DataItem item1 = new DataItem(3);
		DataItem item3 = new DataItem(16);
		DataItem item2 = new DataItem(5);
		hashTable.insert(item3);
		hashTable.insert(item1);
		hashTable.insert(item2);
		Random n = new Random();
		for(int i=0; i<(arraySize/2); i++){
			DataItem item = new DataItem(n.nextInt(arraySize*100));
			hashTable.insert(item);
		}
		
		hashTable.display();
		
		hashTable.delete(item1);
		
		hashTable.display();
	}
}