import java.util.*;

class DataItem{
	
	private int key;
	private int data;
	
	
	public DataItem(int data){
		this.data = data;
		key = data;
	}
	
	public int getKey(){
		return key;
	}
	
	public int getData(){
		return data;
	}
	
}

class HTable{
	
	private int arraySize;
	private DataItem[] hashArray;
	
	public HTable(int arraySize){
		this.arraySize = arraySize;
		hashArray = new DataItem[arraySize];
	}
	
	public void insert(DataItem item){
		int index = hash(item.getKey());
		int step = stepHash(item.getKey());
		while(hashArray[index] != null){
			index+=step;
			index%=arraySize;
		}
		hashArray[index] = item;
	}
	
	public DataItem delete(DataItem item){
		int index = hash(item.getKey());
		int step = stepHash(item.getKey());
		while(hashArray[index] != item){
			if(hashArray[index] == null){
				return null;
			}
			index+=step;
			index %= arraySize;
		}
		DataItem tmp = hashArray[index];
		hashArray[index] = null;
		return tmp;
	}
	
	public DataItem find(DataItem item){
		int index = hash(item.getKey());
		int step = stepHash(item.getKey());
		while(hashArray[index] != item){
			if(hashArray[index] == null){
				return null;
			}
			index+=step;
			index%=arraySize;
		}
		return hashArray[index];
	}
	
	public int hash(int key){
		return (key%arraySize);
	}
	
	public int stepHash(int key){
		return(5 - key%5);
	}
	
	public void display(){
		for(int i=0; i<arraySize; i++){
			if(hashArray[i] == null){
				System.out.println("index [" + i + "]: null");
			}else{
				System.out.println("index [" + i + "]: " + hashArray[i].getData());
			}
		}
	}
	
}

public class HTableApp{
	public static void main(String[] args){
		
		Scanner in = new Scanner(System.in);
		int arraySize;
		
		System.out.println("Enter size of hash array:");
		arraySize = Integer.parseInt(in.nextLine());
		
		HTable hashTable = new HTable(arraySize);
		DataItem item1 = new DataItem(3);
		DataItem item2 = new DataItem(5);
		hashTable.insert(item1);
		hashTable.insert(item2);
		Random n = new Random();
		for(int i=0; i<(arraySize/2); i++){
			DataItem item = new DataItem(n.nextInt(arraySize*100));
			hashTable.insert(item);
		}
		
		hashTable.display();
	}
}