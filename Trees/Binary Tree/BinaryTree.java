class Node{
	private int key;
	private int data;
	private Node leftChild;
	private Node rightChild;
	
	public Node(int key, int data){
		this.key = key;
		this.data = data;
	}
	
	public void setKey(int key){
		this.key = key;
	}
	
	public int getKey(){
		return this.key;
	}
	
	public void setData(int data){
		this.data = data;
	}
	
	public int getData(){
		return this.data;
	}
	
	public void setLeftChild(Node leftChild){
		this.leftChild = leftChild;
	}
	
	public Node getLeftChild(){
		return this.leftChild;
	}
	
	public void setRightChild(Node rightChild){
		this.rightChild = rightChild;
	}
	
	public Node getRightChild(){
		return this.rightChild;
	}
}

class BTree{
	
	private Node root;
	
	public Node find(int key){
		Node current = root;
		while(current.getKey() != key){
			if(key < current.getKey()){
				current = current.getLeftChild();
			}else{
				current = current.getRightChild();
			}
			if(current == null){
				System.out.println("Node does not exist!");
				return null;
			}
		}
		System.out.println("Node has been found!");
		return current;
	}
	
	public void insert(int key, int data){
	
		Node n = new Node(key, data);
		if(root == null){
			root = n;
			System.out.println("Root = " + root.getKey());
		}else{
			Node current = root;
			Node parent = root;
			while(current != null){
				parent = current;
				if(current.getKey() > key){
					current = current.getLeftChild();
				}else if(current.getKey() < key){
					current = current.getRightChild();
				}else{
					System.err.println("The key " + key + " already exists. Please enter a unique key!");
					return;
				}
			}
			current = n;
			if(key > parent.getKey()){
				parent.setRightChild(current);
			}else{
				parent.setLeftChild(current);
			}
		}
	}
	
	public void traverse(){
		System.out.print("in-order traversal: ");
		in_order(root);
		System.out.println("");
		
		System.out.print("pre-order traversal: ");
		pre_order(root);
		System.out.println("");
		System.out.print("post-order traversal: ");
		post_order(root);
		System.out.println("");
		
	}
	
	public void in_order(Node node){
		if(node == null){
			return;
		}
		in_order(node.getLeftChild());
		System.out.print(node.getData() + " ");
		in_order(node.getRightChild());
	}
	
	public void pre_order(Node node){
		
		if(node==null){
			return;
		}
		System.out.print(node.getData() + " ");
		pre_order(node.getLeftChild());
		pre_order(node.getRightChild());
	}
	
	public void post_order(Node node){
		
		if(node==null){
			return;
		}
		post_order(node.getLeftChild());
		post_order(node.getRightChild());
		System.out.print(node.getData() + " ");
	}
	
	public Node get_min(){
		
		Node current = root;
		while(current.getLeftChild() != null){
			current = current.getLeftChild();
		}
		return current;
	}
	
	public Node get_max(){
		
		Node current = root;
		while(current.getRightChild() != null){
			current = current.getRightChild();
		}
		return current;
	}
	
	public void delete(int key){
		Node current = root;
		Node parent = root;
		
		while(current.getKey() != key){
			parent = current;
			if(current.getKey() > key){
				current = current.getLeftChild();
			}else{
				current = current.getRightChild();
			}
			if(current == null){
				System.out.println(key + " does not exist");
				return;
			}
		}
		
		if(current.getLeftChild() == null && current.getRightChild() == null){
			if(current == root){
				root = null;
			}else if(parent.getKey() > key){
				parent.setLeftChild(null);
			}else{
				parent.setRightChild(null);
			}
		}else if(current.getRightChild() == null){
			if(current == root){
				root = current.getLeftChild();
			}else{
				if(parent.getKey() > key){
					parent.setLeftChild(current.getLeftChild());
				}else{
					parent.setRightChild(current.getLeftChild());
				}
			}
		}else if(current.getLeftChild() == null){
			if(current == root){
				root = current.getRightChild();
			}else{
				if(parent.getKey() > key){
					parent.setLeftChild(current.getRightChild());
				}else{
					parent.setRightChild(current.getRightChild());
				}
			}
		}else{
			
			Node successor = current.getRightChild();
			Node parent_successor = current;
			
			while(successor.getLeftChild() != null){
				parent_successor = successor;
				successor = successor.getLeftChild();
			}
			
			if(successor != current.getRightChild()){
				parent_successor.setLeftChild(successor.getRightChild());
				successor.setLeftChild(current.getLeftChild());
				successor.setRightChild(current.getRightChild());
			}else{
				successor.setLeftChild(current.getLeftChild());
			}
			if(current == root){
					root = successor;
				}else if(parent.getKey() > key){
					parent.setLeftChild(successor);
				}else{
					parent.setRightChild(successor);
				}
		}

	}
}

public class BinaryTree{
	
	public static void main(String[] args){
		BTree btree = new BTree();
		btree.insert(5,5);
		btree.insert(7,7);
		btree.insert(3,3);
		btree.insert(1,1);
		btree.insert(4,4);
		btree.insert(6,6);
		btree.insert(2,2);
		btree.insert(12,12);
		btree.insert(9,9);
		btree.insert(8,8);
		btree.insert(14,14);
		
		btree.traverse();
		
		Node min = btree.get_min();
		System.out.println("Min: Key = " + min.getKey() + " Data = " + min.getData());
		Node max = btree.get_max();
		System.out.println("Max: Key = " + max.getKey() + " Data = " + max.getData());
		
		btree.delete(5);
		btree.traverse();
		
		btree.delete(12);
		btree.traverse();
		btree.delete(3);
		btree.traverse();
		btree.delete(5);
		btree.traverse();
	}
}